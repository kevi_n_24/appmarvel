//
//  Pokemon.swift
//  appMarvel
//
//  Created by Kevin on 29/11/17.
//  Copyright © 2017 Kevin. All rights reserved.
//

import Foundation
import ObjectMapper

class Pokemon: Mappable {
    
    
    var name: String?
    var height: Double?
    var weight: Double?
    var id:Int?

    required init?(map: Map) {
    }
    
    func mapping(map: Map){
        
        name <- map["name"]
        height <- map["height"]
        weight <- map["weight"]
        id <- map["id"]
        
    }
    
}
