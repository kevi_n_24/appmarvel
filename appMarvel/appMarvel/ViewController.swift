//
//  ViewController.swift
//  appMarvel
//
//  Created by Kevin on 28/11/17.
//  Copyright © 2017 Kevin. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class ViewController: UIViewController {

    
    
    
    
    
    
    
    //MARK:- Outlet
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    
    @IBOutlet weak var heightLabel: UILabel!
    
    
    
    
   
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Actions
    @IBAction func btnConsultar(_ sender: Any) {
        
//        Alamofire.request("https://pokeapi.co/api/v2/pokemon/1/").responseJSON { response in
//
//
//            if let json = response.result.value {
//                print("JSON: \(json)") //
//            }
//
//        }
        let pkId = arc4random_uniform(250) + 1

        Alamofire.request("https://pokeapi.co/api/v2/pokemon/\(pkId)").responseObject { (response: DataResponse<Pokemon>) in

            let pokemon = response.result.value
            DispatchQueue.main.async {
                self.nameLabel.text = pokemon?.name ?? ""
                self.heightLabel.text = "\(pokemon?.height ?? 0)"
                self.weightLabel.text = "\(pokemon?.weight ?? 0)"
                


            }
        }
        
        
    }
  
    


}

