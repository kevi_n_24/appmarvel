//
//  DetailViewController.swift
//  appMarvel
//
//  Created by Kevin on 3/1/18.
//  Copyright © 2018 Kevin. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    var pokemon:Pokemon?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        print(pokemon?.name)
        self.title = pokemon?.name
        let pkService = pokemonService()
        
        pkService.getPokemonImage(id: (pokemon?.id)!){ (pkImage) in
            self.imagenPK.image = pkImage
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
      // MARK: - Outlet
    
    @IBOutlet weak var imagenPK: UIImageView!
    
}
