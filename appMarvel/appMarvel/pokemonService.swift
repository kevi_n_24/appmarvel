//
//  pokemonService.swift
//  appMarvel
//
//  Created by Kevin on 2/1/18.
//  Copyright © 2018 Kevin. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import AlamofireImage

protocol PokemonServiceDelegate{
    func get20FirstPokemons(pokemons:[Pokemon])
}

class pokemonService{
    
    var deletage:PokemonServiceDelegate?
    
    func getPokemonImage(id:Int , completion:@escaping (UIImage)->()){
        Alamofire.request("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/\(id).png").responseImage { response in
            debugPrint(response)
            
            
            
            if let image = response.result.value {
                //print("image downloaded: \(image)")
                completion(image)
            }
        }
    }
    
    func downloadPokemons(){
        var pokemonArray:[Pokemon] = []
        let dpGR = DispatchGroup()
        for i in 1...20{
            dpGR.enter()
            Alamofire.request("https://pokeapi.co/api/v2/pokemon/\(i)").responseObject { (response: DataResponse<Pokemon>) in
                
                let pokemon = response.result.value
                pokemonArray.append(pokemon!)
                //print(pokemonArray)
                dpGR.leave()
            }
        }
        dpGR.notify(queue: .main){
            let sortedArray = pokemonArray.sorted(by: {$0.id! < $1.id!})
            self.deletage?.get20FirstPokemons(pokemons: sortedArray)
//            pokemonArray = pokemonArray.sorted(by: {$0.id! < $1.id!})
//            self.deletage?.get20FirstPokemons(pokemons: pokemonArray)
        }
    }
    
    
}
