//
//  PokedexViewController.swift
//  appMarvel
//
//  Created by Kevin on 29/11/17.
//  Copyright © 2017 Kevin. All rights reserved.
//

import UIKit

class PokedexViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,PokemonServiceDelegate {

    
    //MARK: - Outlet
    @IBOutlet weak var pokedexTableView: UITableView!
    var pokemonArray:[Pokemon] = []
    var pokemonIndex = 0
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let servicio = pokemonService()
        servicio.deletage = self // responde
        servicio.downloadPokemons()
        //pokedexTableView.reloadData()
        

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
    func   get20FirstPokemons(pokemons: [Pokemon]) {
        pokemonArray = pokemons
        pokedexTableView.reloadData()
    }


     //MARK: - Table
    
   
    
    func numberOfSections(in tableView: UITableView) -> Int {
        //numero de secciones
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
//numero de filas en cada seccion
        
//        switch section {
//        case 0:
//            return 5
//        default:
//            return 10
//        }
        return pokemonArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //devuelve la data de cada fila 
//        let cell = UITableViewCell()
//        cell.textLabel?.text = pokemonArray[indexPath.row].name
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "pokemonCell") as! PokemonTableViewCell // casting a table view cell
        cell.fillData(pokemon: pokemonArray[indexPath.row])
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        switch section {
//        case 0:
//            return "section 1"
//        default:
//            return "section 2"
//        }
        return "Pokemon"
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        pokemonIndex = indexPath.row
        return indexPath
    }
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        pokemonIndex = indexPath.row
//    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) { // envio a otra pantalla
        let pokemonDetail = segue.destination as! DetailViewController
        pokemonDetail.pokemon = pokemonArray[pokemonIndex]
    }
    
    
    
    

}
