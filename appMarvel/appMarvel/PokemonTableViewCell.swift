//
//  PokemonTableViewCell.swift
//  appMarvel
//
//  Created by Kevin on 3/1/18.
//  Copyright © 2018 Kevin. All rights reserved.
//

import UIKit

class PokemonTableViewCell: UITableViewCell {

    //MARK: - Outlet
    
    @IBOutlet weak var namePokemonLabel: UILabel!
    @IBOutlet weak var pesoPokemonLabel: UILabel!
    @IBOutlet weak var alturaPokemonLabel: UILabel!
    
    //var pokemon:Pokemon
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    
    
   // funcion
    func fillData(pokemon:Pokemon){
        namePokemonLabel.text = pokemon.name
        pesoPokemonLabel.text = "\(pokemon.weight ?? 0)"
        alturaPokemonLabel.text = "\(pokemon.height ?? 0)"
    }
    
}
